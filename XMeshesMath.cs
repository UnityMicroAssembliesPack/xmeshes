﻿using UnityEngine;

internal class XMeshesMath
{
    public struct TwoPointsMeshTransformCorrection {
        public TwoPointsMeshTransformCorrection(
            bool inYAxisOrientation = false, bool inMiddleCenterPlacement = false
        ) : this(inYAxisOrientation, inMiddleCenterPlacement, Vector3.one) { }

        public TwoPointsMeshTransformCorrection(
            bool inYAxisOrientation, bool inMiddleCenterPlacement, Vector3 inScaleCorrection)
        {
            yAxisOrientation = inYAxisOrientation;
            middleCenterPlacement = inMiddleCenterPlacement;
            scaleCorrection = inScaleCorrection;
        }

        public bool yAxisOrientation;
        public bool middleCenterPlacement;
        public Vector3 scaleCorrection;
    }

    public static void computeTransformByTwoPoints(
        out XMathTypes.TransformData outTransformData,
        Vector3 inStart, Vector3 inEnd)
    {
        computeTransformByTwoPoints(out outTransformData, inStart, inEnd,
            new TwoPointsMeshTransformCorrection()
        );
    }

    public static void computeTransformByTwoPoints(
        out XMathTypes.TransformData outTransformData,
        Vector3 inStart, Vector3 inEnd,
        TwoPointsMeshTransformCorrection inCorrection)
    {
        outTransformData = new XMathTypes.TransformData();

        Vector3 theDiraction = inEnd - inStart;
        float theLength = theDiraction.magnitude;

        outTransformData.position = inStart;
        if (inCorrection.middleCenterPlacement) {
            outTransformData.position +=
                theDiraction.normalized * theLength * 0.5f;
        }

        outTransformData.rotation = Quaternion.LookRotation(theDiraction, Vector3.up);
        if (inCorrection.yAxisOrientation) {
            outTransformData.rotation *= Quaternion.Euler(90.0f, 0.0f, 0.0f);
        }
 
        outTransformData.scale = new Vector3(1.0f, theLength, 1.0f);
        XMath.perComponentMultiply(
            ref outTransformData.scale, inCorrection.scaleCorrection
        );
    }
}
