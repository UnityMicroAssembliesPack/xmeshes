﻿using UnityEngine;

public class CylinderObject : PrimitiveObject
{
    // ---API---

    public void set(Vector3 inStart, Vector3 inEnd, float inDiameter = 1.0f) {
        XMathTypes.TransformData theTransformData;
        XMeshesMath.TwoPointsMeshTransformCorrection theCorrection =
            new XMeshesMath.TwoPointsMeshTransformCorrection(
                true, true, new Vector3(1.0f, 0.5f, 1.0f)
            );
        XMeshesMath.computeTransformByTwoPoints(
            out theTransformData, inStart, inEnd, theCorrection
        );
        theTransformData.scale.x = theTransformData.scale.z = inDiameter;

        theTransformData.applyForTransform(transform, XMathTypes.Locality.Global);
    }

    // ---Implementation---
    private void Awake() {
        initWithPrimitive(XMeshes.Primitive.Cylinder);
    }

    ////Fields for debug {
    ////TODO: Use external Editor-Only component better?
    //private void OnValidate() {
    //    set(_debug_start, _debug_end);
    //}
    //[SerializeField] private Vector3 _debug_start = new Vector3(0.0f, 0.0f, 0.0f);
    //[SerializeField] private Vector3 _debug_end = new Vector3(0.0f, 1.0f, 0.0f);
    ////}
}
