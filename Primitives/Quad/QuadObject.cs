﻿using UnityEngine;

public class QuadObject : PrimitiveObject
{
    // ---API---

    public void set(Vector3 inOrigin, Vector3 inNormal, Vector2 inSize) {
        var theTransformData = new XMathTypes.TransformData();
        
        theTransformData.position = inOrigin;
        
        theTransformData.rotation =
                Quaternion.FromToRotation(Vector3.up, inNormal) *
                Quaternion.Euler(90.0f, 0.0f, 0.0f);
        
        theTransformData.scale = new Vector3(inSize.x, inSize.y, 1.0f);
        
        transform.position = theTransformData.position;
        transform.rotation = theTransformData.rotation;
        transform.localScale = theTransformData.scale;        
    }

    public void setVisible(bool inIsVisible) {
        gameObject.SetActive(inIsVisible);
    }

    // ---Implementation---
    private void Awake() {
        initWithPrimitive(XMeshes.Primitive.Quad);
    }

    ////Fields for debug {
    ////TODO: Use external Editor-Only component better?
    //private void OnValidate() {
    //    set(_debug_origin, _debug_normal, _debug_size);
    //}
    //[SerializeField] private Vector3 _debug_origin = new Vector3(0.0f, 0.0f, 0.0f);
    //[SerializeField] private Vector3 _debug_normal = new Vector3(0.0f, 1.0f, 0.0f);
    //[SerializeField] private Vector2 _debug_size = new Vector2(1.0f, 1.0f);
    ////}
}

// ==================================================================================

[System.Serializable] public class CT_QuadObject : XChangesTracking.CT_Ref<QuadObject> { }
