﻿using UnityEngine;
using static XUtils;

public class LineObject : MonoBehaviour
{
    // ---API---

    public void set(Vector3 inPointA, Vector3 inPointB, float inThickness) {
        getLineRender().SetPosition(0, transform.InverseTransformPoint(inPointA));
        getLineRender().SetPosition(1, transform.InverseTransformPoint(inPointB));

        getLineRender().startWidth = inThickness;
        getLineRender().endWidth = inThickness;

        getLineRender().receiveShadows = false;
        getLineRender().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
    }

    public void setColor(Color inColor) {
        getLineRender().startColor = inColor;
        getLineRender().endColor = inColor;
    }

    public void setVisibilityVisual(bool inIsVisivle) {
        XUtils.setVisibilityVisual(this, inIsVisivle);
    }

    public void setVisibilityInHierarchy(bool inIsVisivle) {
        XUtils.setVisibilityInHierarchy(this, inIsVisivle);
    }

    // ---Implementation---
    private LineRenderer getLineRender() {
        return getComponent<LineRenderer>(this, AccessPolicy.ShouldExist);
    }

    //Fields
    private LineRenderer _lineRender = null;
}
