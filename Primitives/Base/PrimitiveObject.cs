﻿using UnityEngine;

[ExecuteAlways]
public class PrimitiveObject : MonoBehaviour
{
    //Methods
    //-API
    public void setColor(Color inColor) {
        Material theMaterial = XUtils.verify(_meshRenderer).sharedMaterial;
        theMaterial.SetColor("_BaseColor", inColor);
    }

    public void setVisibilityVisual(bool inIsVisible) {
        XUtils.setVisibilityVisual(gameObject, inIsVisible);
    }

    public void setVisibilityInHierarchy(bool inIsVisible) {
        XUtils.setVisibilityInHierarchy(gameObject, inIsVisible);
    }

    //-Child API
    protected void initWithMesh(Mesh inMesh) {
        XUtils.check(inMesh);

        _meshFilter = XUtils.getComponent<MeshFilter>(
            this, XUtils.AccessPolicy.ShouldExist
        );
        _meshRenderer = XUtils.getComponent<MeshRenderer>(
            this, XUtils.AccessPolicy.ShouldExist
        );

        _meshFilter.mesh = inMesh;
        _meshRenderer.sharedMaterial = new Material(Shader.Find("HDRP/Lit"));
    }

    protected void initWithPrimitive(XMeshes.Primitive inPrimitive) {
        initWithMesh(XInfrastructure.getManager<XMeshesManager>().getPrimitive(inPrimitive));
    }

    //Fields
    private MeshFilter _meshFilter = null;
    private MeshRenderer _meshRenderer = null;
}
