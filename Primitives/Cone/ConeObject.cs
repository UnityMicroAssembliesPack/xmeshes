﻿using UnityEngine;

public class ConeObject : PrimitiveObject
{
    public void set(Vector3 inStart, Vector3 inEnd, float inDiameter = 1.0f) {
        XMathTypes.TransformData theTransformData;
        XMeshesMath.TwoPointsMeshTransformCorrection theCorrection =
            new XMeshesMath.TwoPointsMeshTransformCorrection(
                true, false
            );
        XMeshesMath.computeTransformByTwoPoints(
            out theTransformData, inStart, inEnd, theCorrection
        );
        theTransformData.scale.x = theTransformData.scale.z = inDiameter;

        theTransformData.applyForTransform(transform, XMathTypes.Locality.Global);
    }

    // ---Implementation---
    private void Awake() {
        initWithPrimitive(XMeshes.Primitive.Cone);
    }
}
