﻿using UnityEngine;
using XMathTypes;

public class ConvexPolygonObject : PrimitiveObject
{
    //Methods
    //-API
    public void set(Vector2[] inVertices, Vector3 inOriginPoint, Vector3 inNormal, Vector3 inXAxisOrientation) {
        if (!XUtils.isValid(inVertices) || inVertices.Length < 3) {
            enabled = false;
            return;
        } else {
            enabled = true;
        }

        updateMeshForConvexPolygon(inVertices);
        orientMeshWith(inOriginPoint, inNormal, inXAxisOrientation);
    }

    public ConvexPolygon2D polygon {
        get { return _polygon; }
    }

    //-Implementation
    private void updateMeshForConvexPolygon(Vector2[] inConvexPolygonVertices) {
        if (null == _mesh) {
            initWithMesh(_mesh = new Mesh());
        }

        XUtils.check(inConvexPolygonVertices);
        XUtils.check(inConvexPolygonVertices.Length >= 3);
        int theVerticesNum = inConvexPolygonVertices.Length;

        //Vertices setup
        Vector3[] theVertices = null;
        Vector2[] theUVs = null;
        int[] theTriangles = null;

        bool theIsMeshChanged = (inConvexPolygonVertices.Length != _mesh.vertices.Length);

        if (theIsMeshChanged) {
            theVertices = new Vector3[theVerticesNum];
            theUVs = new Vector2[theVerticesNum];
            theTriangles = new int[(theVerticesNum - 2) * 3];
        } else {
            theVertices = _mesh.vertices;
            theUVs = _mesh.uv;
            theTriangles = _mesh.triangles;
        }

        //Update vertices
        for (int theIndex = 0; theIndex < theVerticesNum; ++theIndex) {
            Vector3 thePolygonVertex = XMath.getVector3From(
                inConvexPolygonVertices[theIndex], TransformPlane.XZ
            );

            ref Vector3 theMeshVertexRef = ref theVertices[theIndex];
            if (!XMath.equalsWithPrecision(theMeshVertexRef, thePolygonVertex)) {
                theMeshVertexRef = thePolygonVertex;
                theIsMeshChanged = true;
            }
        }

        if (!theIsMeshChanged) return;

        _polygon = new ConvexPolygon2D(inConvexPolygonVertices, true);

        //Update UVs
        Vector2 theMinComponents = inConvexPolygonVertices[0];
        Vector2 theMaxComponents = inConvexPolygonVertices[0];
        for (int theIndex = 1; theIndex < theVerticesNum; ++theIndex) {
            Vector2 theVertex = inConvexPolygonVertices[theIndex];
            if (theVertex.x < theMinComponents.x) {
                theMinComponents.x = theVertex.x;
            } else if (theVertex.x > theMaxComponents.x) {
                theMaxComponents.x = theVertex.x;
            }

            if (theVertex.y < theMinComponents.y) {
                theMinComponents.y = theVertex.y;
            } else if (theVertex.y > theMaxComponents.y) {
                theMaxComponents.y = theVertex.y;
            }
        }

        Vector2 theComponentsRange = theMaxComponents - theMinComponents;
        for (int theIndex = 0; theIndex < theVerticesNum; ++theIndex) {
            Vector2 theVertexMinRelated = inConvexPolygonVertices[theIndex] - theMinComponents;
            theUVs[theIndex] = XMath.perComponentDivision(theVertexMinRelated, theComponentsRange);
        }

        //Update triangles
        for (int theTriangleIndex = 0; theTriangleIndex < theVerticesNum - 2; ++theTriangleIndex) {
            int theTriangleOffset = theTriangleIndex * 3;
            theTriangles[theTriangleOffset + 0] = 0;
            theTriangles[theTriangleOffset + 1] = theTriangleIndex + 1;
            theTriangles[theTriangleOffset + 2] = theTriangleIndex + 2;
        }

        //Apply changes
        _mesh.vertices = theVertices;
        _mesh.uv = theUVs;
        _mesh.triangles = theTriangles;
        _mesh.RecalculateBounds();
        _mesh.RecalculateNormals();
    }

    private void orientMeshWith(
        Vector3 inOriginPoint, Vector3 inNormal, Vector3 inXAxisOrientation)
    {
        new LocatorTransform(inOriginPoint, inNormal, inXAxisOrientation).applyForTransform(
            transform
        );
    }

    //Fields
    private Mesh _mesh = null;

    private ConvexPolygon2D _polygon;
}
