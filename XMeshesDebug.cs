﻿using UnityEngine;

internal class XMeshesDebug
{
    static public void printMesh(Mesh inMesh) {
        if (!inMesh) {
            Debug.Log("<No meshes>");
            return;
        }

        Debug.Log("Vertices:");
        for (int theIndex = 0; theIndex < inMesh.vertexCount; ++theIndex) {
            Debug.Log("\t[" + theIndex + "]: " + inMesh.vertices[theIndex].ToString());
        }

        Debug.Log("Indices:");
        int theTrianglesNum = inMesh.triangles.Length / 3;
        for (int theTriangleIndex = 0; theTriangleIndex < theTrianglesNum;
            ++theTriangleIndex)
        {
            int theTriangleOffset = theTriangleIndex * 3;
            Debug.Log("\t[" + theTriangleIndex + "]: "
                + inMesh.triangles[theTriangleOffset + 0] + " , "
                + inMesh.triangles[theTriangleOffset + 1] + " , "
                + inMesh.triangles[theTriangleOffset + 2]
            );
        }

        Debug.Log("UVs:");
        for (int theIndex = 0; theIndex < inMesh.vertexCount; ++theIndex) {
            Debug.Log("\t[" + theIndex + "]: " + inMesh.uv[theIndex].ToString());
        }
    }
}
