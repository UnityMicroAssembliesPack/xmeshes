﻿using UnityEngine;

/*
public class XMeshesManager
{
    public enum Primitive {
        Capsule,
        Cone,
        Cube,
        Cylinder,
        Plane,
        Quad,
        Sphere,
        None
    }

    static public Mesh getPrimitive(Primitive inPrimitive) {
        XUtils.check(Primitive.None != inPrimitive);

        return _primitiveMeshes.findOrCreateElement(
            (PrimitivePair inPair)=>{ return inPair.primitive == inPrimitive; },
            () => { return new PrimitivePair(inPrimitive, createMesh(inPrimitive)); }
        ).mesh;
    }

    // ============================= Implementation =================================

    static private Mesh createMesh(Primitive inPrimitive) {
        GameObject theUnityPrimitive = null;

        switch (inPrimitive)
        {
            case Primitive.Capsule:    theUnityPrimitive = GameObject.CreatePrimitive(PrimitiveType.Capsule);  break;
            case Primitive.Cone:       return createDefaultConeMesh();
            case Primitive.Cube:       theUnityPrimitive = GameObject.CreatePrimitive(PrimitiveType.Cube);     break;
            case Primitive.Cylinder:   theUnityPrimitive = GameObject.CreatePrimitive(PrimitiveType.Cylinder); break;
            case Primitive.Plane:      theUnityPrimitive = GameObject.CreatePrimitive(PrimitiveType.Plane);    break;
            case Primitive.Quad:       theUnityPrimitive = GameObject.CreatePrimitive(PrimitiveType.Quad);     break;
            case Primitive.Sphere:     theUnityPrimitive = GameObject.CreatePrimitive(PrimitiveType.Sphere);   break;
            default:                   XUtils.check(false); return null;
        }

        Mesh theMesh = XUtils.getComponent<MeshFilter>(
            theUnityPrimitive, XUtils.AccessPolicy.ShouldExist
        ).sharedMesh;
        GameObject.DestroyImmediate(theUnityPrimitive);
        return theMesh;
    }

    // - - - - - Cone mesh - - - - - -

    static private Mesh createDefaultConeMesh() {
        return createConeMesh(10, 0.5f, 1.0f);
    }
    static private Mesh createConeMesh(
        int inSides, float inRadius, float inHeight)
    {
        Mesh mesh = new Mesh();

        Vector3[] vertices = new Vector3[inSides + 2];
        Vector2[] uv = new Vector2[vertices.Length];
        int[] triangles = new int[(inSides * 2) * 3];

        vertices[0] = Vector3.zero;
        uv[0] = new Vector2(0.5f, 0f);
        for (int i = 0, n = inSides - 1; i < inSides; i++)
        {
            float ratio = (float)i / n;
            float r = ratio * (Mathf.PI * 2f);
            float x = Mathf.Cos(r) * inRadius;
            float z = Mathf.Sin(r) * inRadius;
            vertices[i + 1] = new Vector3(x, 0f, z);

            uv[i + 1] = new Vector2(ratio, 0f);
        }
        vertices[inSides + 1] = new Vector3(0f, inHeight, 0f);
        uv[inSides + 1] = new Vector2(0.5f, 1f);

        // construct bottom

        for (int i = 0, n = inSides - 1; i < n; i++) {
            int offset = i * 3;
            triangles[offset] = 0;
            triangles[offset + 1] = i + 1;
            triangles[offset + 2] = i + 2;
        }

        // construct sides

        int bottomOffset = inSides * 3;
        for (int i = 0, n = inSides - 1; i < n; i++) {
            int offset = i * 3 + bottomOffset;
            triangles[offset] = i + 1;
            triangles[offset + 1] = inSides + 1;
            triangles[offset + 2] = i + 2;
        }

        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = triangles;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        return mesh;
    }

    //Fields
    private struct PrimitivePair : System.IEquatable<PrimitivePair> {
        public PrimitivePair(Primitive inPrimitive, Mesh inMesh) {
            primitive = inPrimitive;
            mesh = inMesh;
        }

        public bool Equals(PrimitivePair inOther) {
            return primitive == inOther.primitive &&
                mesh == inOther.mesh;
        }

        public Primitive primitive;
        public Mesh mesh;
    }
    static private FastArray<PrimitivePair> _primitiveMeshes = new FastArray<PrimitivePair>();
}
*/
