﻿using UnityEngine;

[ExecuteAlways]
public class BasisObject : MonoBehaviour
{
    //Methods
    //-API
    public void set(XMathTypes.LocatorTransform inTransform) {
        if (0.0f == _size) return;

        Vector3 thePosition = inTransform.position;
        _xAxisArrow.set(thePosition, inTransform.getGlobalPoint(_size * Vector3.right));
        _yAxisArrow.set(thePosition, inTransform.getGlobalPoint(_size * Vector3.up));
        _zAxisArrow.set(thePosition, inTransform.getGlobalPoint(_size * Vector3.forward));
    }

    public void setVisibilityVisual(bool inIsVisible) {
        XUtils.setVisibilityVisual(gameObject, inIsVisible);
    }

    public void setVisibilityInHierarchy(bool inIsVisible) {
        XUtils.setVisibilityInHierarchy(gameObject, inIsVisible);
    }

    //-Implementation
    private void Start() {
        XUtils.verify(_xAxisArrow).setColor(Color.red);
        XUtils.verify(_yAxisArrow).setColor(Color.green);
        XUtils.verify(_zAxisArrow).setColor(Color.blue);

        _xAxisArrow.setVisibilityInHierarchy(false);
        _yAxisArrow.setVisibilityInHierarchy(false);
        _zAxisArrow.setVisibilityInHierarchy(false);
    }

    //Fields
    [SerializeField] private float _size = 1.0f;
    [SerializeField] private ArrowObject _xAxisArrow = null;
    [SerializeField] private ArrowObject _yAxisArrow = null;
    [SerializeField] private ArrowObject _zAxisArrow = null;
}

// ==================================================================================

[System.Serializable] public class CT_BasisObject : XChangesTracking.CT_Ref<BasisObject> { }
