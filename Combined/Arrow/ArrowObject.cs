﻿using UnityEngine;

public class ArrowObject : MonoBehaviour
{
    //Methods
    //-API
    public void set(Vector3 inStart, Vector3 inEnd) {
        set(inStart, inEnd, ArrowSettings.getDefault());
    }

    public void set(Vector3 inStart, Vector3 inEnd,
        ArrowSettings inArrowSettings)
    {
        Vector3 theDiraction = inEnd - inStart;
        float theLength = theDiraction.magnitude;
        Vector3 theConeStart = inStart +
            theDiraction.normalized * (theLength - inArrowSettings.coneHeight);

        _cone.set(theConeStart, inEnd, inArrowSettings.coneDiamiter);
        _cylinder.set(inStart, theConeStart, inArrowSettings.cylinderDiamiter);
    }

    public void setColor(Color inColor) {
        _cone.setColor(inColor);
        _cylinder.setColor(inColor);
    }

    public void setVisibilityVisual(bool inIsVisible) {
        XUtils.setVisibilityVisual(gameObject, inIsVisible);
    }

    public void setVisibilityInHierarchy(bool inIsVisible) {
        XUtils.setVisibilityInHierarchy(gameObject, inIsVisible);
    }

    public struct ArrowSettings {
        public static ArrowSettings getDefault() {
            ArrowSettings theResult = new ArrowSettings();
            theResult.cylinderDiamiter = 0.1f;
            theResult.coneHeight = 0.2f;
            theResult.coneDiamiter = 0.2f;

            return theResult;
        }

        public ArrowSettings(float inConeHeight, float inConeDiamiter,
            float inCylinderDiamiterPortion = 0.1f)
        {
            cylinderDiamiter = inConeDiamiter * inCylinderDiamiterPortion;

            coneHeight = inConeHeight;
            coneDiamiter = inConeDiamiter;
        }

        internal float cylinderDiamiter;

        internal float coneHeight;
        internal float coneDiamiter;
    }

    //-Implementation
    private void Awake() {
        _cone.setVisibilityInHierarchy(false);
        _cylinder.setVisibilityInHierarchy(false);
    }

    //Fields
    [SerializeField, PrefabModeOnly] private ConeObject _cone = null;
    [SerializeField, PrefabModeOnly] private CylinderObject _cylinder = null;
}
