﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class VolumeObject : MonoBehaviour
{
    //Methods
    //-API
    public void set(XMathTypes.LocatorTransform inTransform,
        XMathTypes.IntVector3 inCellsOffset, XMathTypes.UintVector3 inCellsNum, Vector3 inCellSize,
        float inThickness = 0.1f, bool inFullShow = false)
    {
        XMathTypes.LocatorTransform theTransformWithOffset = inTransform;
        Vector3 theOffsetLocal = XMathTypes.IntVector3.perComponentMultiply(inCellsOffset, inCellSize);
        theTransformWithOffset.position += theTransformWithOffset.getGlobalDiraction(theOffsetLocal);

        updateVolume(theTransformWithOffset, inCellsNum, inCellSize, inThickness, inFullShow);
    }

    public void set(XMathTypes.LocatorTransform inTransform,
        XMathTypes.UintVector3 inCellsNum, Vector3 inCellSize,
        float inThickness = 0.1f, bool inFullShow = false)
    {
        updateVolume(inTransform, inCellsNum, inCellSize, inThickness, inFullShow);
    }

    public void setColor(Color inColor) {
        _color = inColor;

        _reg_linesList.Clear();
        GetComponentsInChildren(_reg_linesList);
        foreach (LineObject theLine in _reg_linesList) {
            theLine.setColor(_color);
        }
    }

    public void setVisibilityVisual(bool inIsVisivle) {
        XUtils.setVisibilityVisual(this, inIsVisivle);
    }

    public void setVisibilityInHierarchy(bool inIsVisivle) {
        XUtils.setVisibilityInHierarchy(this, inIsVisivle);
    }

    //-Implementation
    private void updateVolume(XMathTypes.LocatorTransform inTransform,
        XMathTypes.UintVector3 inCellsNum, Vector3 inCellSize,
        float inThickness = 0.1f, bool inFullShow = false)
    {
        bool theIsValid = true;
        theIsValid &= (inCellsNum.x > 0) && (inCellsNum.y > 0) && (inCellsNum.z > 0);
        theIsValid &= (inCellSize.x > 0.0f) && (inCellSize.y > 0.0f) && (inCellSize.z > 0.0f);
        theIsValid &= (inThickness > 0.0f);
        if (!theIsValid) return;

        //Update lines
        if (inFullShow) {
            updateVolumeFullShow(inTransform, inCellsNum, inCellSize, inThickness);
        } else {
            updateVolumeBoundOnly(inTransform, inCellsNum, inCellSize, inThickness);
        }
    }

    private void updateVolumeFullShow(XMathTypes.LocatorTransform inTransform,
        XMathTypes.UintVector3 inCellsNum, Vector3 inCellSize,
        float inThickness)
    {
        uint theActualLinesNum = (inCellsNum.x + 1) * (inCellsNum.z + 1) +
            ((inCellsNum.x + 1) + (inCellsNum.z + 1)) * (inCellsNum.y + 1);
        updateLineObjects(_reg_linesFastArray, theActualLinesNum);
        int theUsingLineIndex = 0;

        //Build volume
        Vector3 thePointA, thePointADelta, thePointBOffset;

        //-Cuts
        Vector3 theVolumeCutOrigin = inTransform.position;
        Vector3 theVolumeCutDelta = inTransform.axisY * inCellSize.y;

        for (int theCutIndex = 0; theCutIndex <= inCellsNum.y; ++theCutIndex) {
            thePointA = theVolumeCutOrigin;
            thePointADelta = inTransform.axisX * inCellSize.x;
            thePointBOffset = inTransform.axisZ * inCellsNum.z * inCellSize.z;
            for (int theXIndex = 0; theXIndex <= inCellsNum.x; ++theXIndex) {
                _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointA + thePointBOffset, inThickness);
                thePointA += thePointADelta;
            }

            thePointA = theVolumeCutOrigin;
            thePointADelta = inTransform.axisZ * inCellSize.z;
            thePointBOffset = inTransform.axisX * inCellsNum.x * inCellSize.x;
            for (int theZIndex = 0; theZIndex <= inCellsNum.z; ++theZIndex) {
                _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointA + thePointBOffset, inThickness);
                thePointA += thePointADelta;
            }

            theVolumeCutOrigin += theVolumeCutDelta;
        }

        //-Cut connection lines
        Vector3 thePointALineOrigin = inTransform.position;
        Vector3 thePointADeltaX = inTransform.axisX * inCellSize.x;
        Vector3 thePointADeltaZ = inTransform.axisZ * inCellSize.z;
        thePointBOffset = inTransform.axisY * inCellsNum.y * inCellSize.y;
        for (int theZIndex = 0; theZIndex <= inCellsNum.z; ++theZIndex) {
            thePointA = thePointALineOrigin;
            for (int theXIndex = 0; theXIndex <= inCellsNum.x; ++theXIndex) {
                _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointA + thePointBOffset, inThickness);
                thePointA += thePointADeltaX;
            }
            thePointALineOrigin += thePointADeltaZ;
        }
    }

    private void updateVolumeBoundOnly(XMathTypes.LocatorTransform inTransform,
        XMathTypes.UintVector3 inCellsNum, Vector3 inCellSize,
        float inThickness)
    {
        uint theActualLinesNum = 4 + 4 + 4;
        updateLineObjects(_reg_linesFastArray, theActualLinesNum);
        int theUsingLineIndex = 0;

        //Build volume
        Vector3 thePointA, thePointB;

        //-Cuts
        Vector3 theXOffset = inTransform.axisX * inCellsNum.x * inCellSize.x;
        Vector3 theYOffset = inTransform.axisY * inCellsNum.y * inCellSize.y;
        Vector3 theZOffset = inTransform.axisZ * inCellsNum.z * inCellSize.z;

        Vector3 theOriginPoint = inTransform.position;
        for (int theYIndex = 0; theYIndex <= 1; ++theYIndex) {
            thePointA = theOriginPoint;
            thePointB = thePointA + theXOffset;
            _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointB, inThickness);

            thePointA = thePointB; thePointB = thePointA + theZOffset;
            _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointB, inThickness);

            thePointA = thePointB; thePointB = thePointA - theXOffset;
            _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointB, inThickness);

            thePointA = thePointB; thePointB = thePointA - theZOffset;
            _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointB, inThickness);

            theOriginPoint += theYOffset;
        }

        //-Cuts connections
        thePointA = inTransform.position;
        _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointA + theYOffset, inThickness);

        thePointA += theXOffset;
        _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointA + theYOffset, inThickness);

        thePointA += theZOffset;
        _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointA + theYOffset, inThickness);

        thePointA -= theXOffset;
        _reg_linesFastArray[theUsingLineIndex++].set(thePointA, thePointA + theYOffset, inThickness);
    }

    private void updateLineObjects(FastArray<LineObject> inLinesArray, uint inLinesNum) {
        _reg_linesList.Clear();
        GetComponentsInChildren(_reg_linesList);

        inLinesArray.assignFrom(_reg_linesList);
        //TODO: Create lines pull
        inLinesArray.setSize((int)inLinesNum,
            ()=>{
                LineObject theNewLine = XUtils.createObject(_linePrefab, transform);
                theNewLine.transform.parent = transform;
                theNewLine.setColor(_color);
                return theNewLine;
            },
            (LineObject inLineObject)=>{
                XUtils.Destroy(inLineObject.gameObject);
            }
        );

        foreach (LineObject theLine in inLinesArray) {
            theLine.setVisibilityInHierarchy(false);
        }
    }
    private List<LineObject> _reg_linesList = new List<LineObject>();

    private FastArray<LineObject> _reg_linesFastArray = new FastArray<LineObject>();

    //Fields
    [SerializeField, PrefabModeOnly] private LineObject _linePrefab = null;

    private Color _color = Color.white;
}
