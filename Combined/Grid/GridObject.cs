﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteAlways]
public class GridObject : MonoBehaviour
{
    //Methods
    //-API
    public void set(Vector3 inOrigin, Vector3 inNormal, Vector3 inColumnsDiraction,
        int inColumnsNum, int inRowsNum, float inColumnSize = 1.0f, float inRowSize = 1.0f,
        float inThickness = 0.1f, bool inDoThicknessOffset = false)
    {
        updateGrid(inOrigin, inNormal, inColumnsDiraction,
            inColumnsNum, inRowsNum, inColumnSize, inRowSize,
            inThickness, inDoThicknessOffset
        );
    }

    public void setColor(Color inColor) {
        _reg_linesList.Clear();
        GetComponentsInChildren(_reg_linesList);
        foreach (LineObject theLine in _reg_linesList) {
            theLine.setColor(inColor);
        }
    }

    public void setVisibilityVisual(bool inIsVisivle) {
        XUtils.setVisibilityVisual(this, inIsVisivle);
    }

    public void setVisibilityInHierarchy(bool inIsVisivle) {
        XUtils.setVisibilityInHierarchy(this, inIsVisivle);
    }

    //-Implementation
    private void updateGrid(Vector3 inOrigin, Vector3 inNormal, Vector3 inColumnsDiraction,
        int inColumnsNum, int inRowsNum, float inColumnSize, float inRowSize,
        float inThickness, bool inDoThicknessOffset)
    {
        XUtils.check(inColumnsNum > 0);
        XUtils.check(inRowsNum > 0);

        //Update lines
        int theActualLinesNum = (inColumnsNum + 1) + (inRowsNum + 1);

        _reg_linesList.Clear();
        GetComponentsInChildren(_reg_linesList);
        _reg_linesFastArray.assignFrom(_reg_linesList);

        _reg_linesFastArray.setSize(theActualLinesNum,
            ()=>{
                LineObject theNewLine = XUtils.createObject(_linePrefab, transform);
                theNewLine.transform.parent = transform;
                //theNewLine.setVisibilityInHierarchy(false);
                return theNewLine;
            },
            (LineObject inLineObject)=>{
                XUtils.Destroy(inLineObject.gameObject);
            }
        );
        int theUsingLineIndex = 0;

        //Build grid
        var theGridTransform = new XMathTypes.LocatorTransform(inOrigin, inNormal, inColumnsDiraction);
        if (inDoThicknessOffset) {
            theGridTransform.position += inNormal * (inThickness / 2);
        }

        Vector3 theLinePointA = theGridTransform.position;
        Vector3 theLinePointADelta = theGridTransform.axisX * inColumnSize;
        Vector3 theLinePointBOffset = theGridTransform.axisZ * inRowsNum * inRowSize;
        for (int theColumnLineIndex = 0; theColumnLineIndex <= inColumnsNum; ++theColumnLineIndex) {
            _reg_linesFastArray[theUsingLineIndex++].set(
                theLinePointA, theLinePointA + theLinePointBOffset, inThickness
            );
            theLinePointA += theLinePointADelta;
        }

        theLinePointA = theGridTransform.position;
        theLinePointADelta = theGridTransform.axisZ * inRowSize;
        theLinePointBOffset = theGridTransform.axisX * inColumnsNum * inColumnSize;
        for (int theRowLineIndex = 0; theRowLineIndex <= inRowsNum; ++theRowLineIndex) {
            _reg_linesFastArray[theUsingLineIndex++].set(
                theLinePointA, theLinePointA + theLinePointBOffset, inThickness
            );
            theLinePointA += theLinePointADelta;
        }
    }
    private List<LineObject> _reg_linesList = new List<LineObject>();
    private FastArray<LineObject> _reg_linesFastArray = new FastArray<LineObject>();

    //Fields
    [SerializeField, PrefabModeOnly] private LineObject _linePrefab = null;
}
